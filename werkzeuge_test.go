package werkzeuge

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"sync"
	"testing"
)

func TestWerkzeuge_RandomString(t *testing.T) {
	var testWerkzeug Werkzeuge

	s := testWerkzeug.RandomString(10)
	if len(s) != 10 {
		t.Error("wrong length random string return")
	}
}

var uploadTests = []struct {
	name             string
	AllowedFileTypes []string
	renameFile       bool
	errorExpected    bool
}{
	{name: "allowed no rename", AllowedFileTypes: []string{"image/jpeg", "image/png"}, renameFile: false, errorExpected: false},
	{name: "allowed rename", AllowedFileTypes: []string{"image/jpeg", "image/png"}, renameFile: true, errorExpected: false},
	{name: "not allowed", AllowedFileTypes: []string{"image/png"}, renameFile: false, errorExpected: true},
}

func TestWerkzeug_UploadFiles(t *testing.T) {

	for _, entry := range uploadTests {
		//set up a pipe to avoid buffering
		pr, pw := io.Pipe()
		writer := multipart.NewWriter(pw)
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			defer writer.Close()
			defer wg.Done()

			//create the form data fiel 'file'
			part, err := writer.CreateFormFile("file", "./testdata/minions.jpeg")
			if err != nil {
				t.Error(err)
			}
			f, err := os.Open("./testdata/minions.jpeg")
			if err != nil {
				t.Error(err)
			}
			defer f.Close()

			img, _, err := image.Decode(f)
			if err != nil {
				t.Errorf("Error decoding image: %s failed", f.Name())
			}
			err = jpeg.Encode(part, img, nil)
			if err != nil {
				t.Error(err)
			}
		}()

		//read from the pipe which receives data
		request := httptest.NewRequest("POST", "/", pr)
		request.Header.Add("Content-Type", writer.FormDataContentType())

		var werkzeug Werkzeuge

		werkzeug.AllowedFileTypes = entry.AllowedFileTypes

		uploadedFiles, err := werkzeug.UploadFiles(request, "./testdata/uploads/", entry.renameFile)
		if err != nil && !entry.errorExpected {
			t.Error(err)
		}

		if !entry.errorExpected {
			if _, err := os.Stat(fmt.Sprintf("./testdata/uploads/%s", uploadedFiles[0].NewFileName)); os.IsNotExist(err) {
				t.Errorf("%s: expected file to exist: %s", entry.name, err.Error())
			}
			//clean up
			_ = os.Remove(fmt.Sprintf("./testdata/uploads/%s", uploadedFiles[0].NewFileName))
		}

		if !entry.errorExpected && err != nil {
			t.Errorf("%s: error expected but none received", entry.name)
		}
		wg.Wait()
	}
}

func TestWerkzeug_CreateDirIfNotExist(t *testing.T) {
	var werkzeug Werkzeuge

	err := werkzeug.CreateDirIfNotExist("./testdata/myDir")
	if err != nil {
		t.Error(err)
	}

	os.Remove("./testdata/myDir")
}

func TestWerkzeug_Slugify(t *testing.T) {
	var werkzeug Werkzeuge
	var res = "hello-world"
	str, err := werkzeug.Slugify("Hello,World!")
	if err != nil {
		t.Error(err)
	}
	if !strings.EqualFold(res, str) {
		t.Error(errors.New(fmt.Sprintf("Slug should be %s, but is %s", res, str)))
	}
}

func TestWerkzeug_DownloadStaticFile(t *testing.T) {
	rr := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)

	var werkzeug Werkzeuge
	werkzeug.DownloadStaticFile(rr, req, "./testdata", "minions.jpeg", "banana.jpeg")

	res := rr.Result()

	defer res.Body.Close()

	if res.Header["Content-Length"][0] != "7766" {
		t.Errorf("Content Length should be 7766 but is %s\n", res.Header["Content-Length"][0])
	}

}

var jsonTests = []struct {
	name          string
	json          string
	errorExpected bool
	maxSize       int
	allowUnknown  bool
}{
	{name: "good json", json: `{"foo": "bar"}`, errorExpected: false, maxSize: 1024, allowUnknown: false},
	{name: "badly formatted json", json: `{"foo":}`, errorExpected: true, maxSize: 1024, allowUnknown: false},
	{name: "incorrect type", json: `{"foo": 1}`, errorExpected: true, maxSize: 1024, allowUnknown: false},
	{name: "two json files", json: `{"foo": "1"}{"alpha": "beta"}`, errorExpected: true, maxSize: 1024, allowUnknown: false},
	{name: "empty body", json: ``, errorExpected: true, maxSize: 1024, allowUnknown: false},
	{name: "syntax error in json", json: `{"foo": 1"`, errorExpected: true, maxSize: 1024, allowUnknown: false},
	{name: "unknown field in json", json: `{"fooo": "1"}`, errorExpected: true, maxSize: 1024, allowUnknown: false},
	{name: "allow unknown fields in json", json: `{"fooo": "1"}`, errorExpected: false, maxSize: 1024, allowUnknown: true},
	{name: "missing field name", json: `{jack: "1"}`, errorExpected: true, maxSize: 1024, allowUnknown: true},
	{name: "file too large", json: `{"foo": "bar"}`, errorExpected: true, maxSize: 5, allowUnknown: true},
	{name: "not json", json: `Hello, world!`, errorExpected: true, maxSize: 1024, allowUnknown: true},
}

func TestWerkzeug_ReadJSON(t *testing.T) {
	var testTool Werkzeuge

	for _, e := range jsonTests {
		// set the max file size
		testTool.MaxJSONSize = e.maxSize

		// allow/disallow unknown fields
		testTool.AllowUnknownJSONFields = e.allowUnknown

		// declare a variable to read the decoded json into
		var decodedJSON struct {
			Foo string `json:"foo"`
		}

		// create a request with the body
		req, err := http.NewRequest("POST", "/", bytes.NewReader([]byte(e.json)))
		if err != nil {
			t.Log("Error:", err)
		}

		// create a recorder
		rr := httptest.NewRecorder()

		err = testTool.ReadJSON(rr, req, &decodedJSON)

		if e.errorExpected && err == nil {
			t.Errorf("%s: error expected, but none received", e.name)
		}

		if !e.errorExpected && err != nil {
			t.Errorf("%s: error not expected, but one received: %s", e.name, err.Error())
		}

		req.Body.Close()
	}
}

func TestWerkzeug_WriteJSON(t *testing.T) {
	var werkzeug Werkzeuge

	rr := httptest.NewRecorder()

	payloud := JSONResponse{
		Error:   false,
		Message: "foo",
	}

	headers := make(http.Header)
	headers.Add("FOO", "BAR")

	err := werkzeug.WriteJSON(rr, http.StatusOK, payloud, headers)
	if err != nil {
		t.Error(err)
	}
}

func TestWerkzeug_ErrorJSOn(t *testing.T) {
	var werkzeug Werkzeuge

	rr := httptest.NewRecorder()
	err := werkzeug.ErrorJSON(rr, errors.New("some error"), http.StatusServiceUnavailable)

	if err != nil {
		t.Error(err)
	}

	var payloud JSONResponse

	decoder := json.NewDecoder(rr.Body)
	err = decoder.Decode(&payloud)
	if err != nil {
		t.Error("received error when decoding JSON", err)
	}

	if !payloud.Error {
		t.Error("error set to false in json, and it should be true")
	}
	if rr.Code != http.StatusServiceUnavailable {
		t.Errorf("wrong status, should be %d but got %d", http.StatusServiceUnavailable, rr.Code)
	}
}

type RoundTripFunc func(req *http.Request) *http.Response

func (f RoundTripFunc) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}

func NewTestClient(fn RoundTripFunc) *http.Client {
	return &http.Client{
		Transport: fn,
	}
}

func TestWerkzeug_PushJSONToRemote(t *testing.T) {
	client := NewTestClient(func(req *http.Request) *http.Response {
		// Test Request Parameters
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       ioutil.NopCloser(bytes.NewBufferString("ok")),
			Header:     make(http.Header),
		}
	})

	var testTools Werkzeuge
	var foo struct {
		Bar string `json:"bar"`
	}
	foo.Bar = "bar"

	_, _, err := testTools.PushJSONToRemote("http://example.com/some/path", foo, client)
	if err != nil {
		t.Error("failed to call remote url:", err)
	}
}
