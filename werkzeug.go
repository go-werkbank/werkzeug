package werkzeuge

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

const randomStringSource = "abcdefghijklmnopqrstuvwxyzABZDEFGHIJKLMNOPQRSTUVWXYZ0123456789_+"

type Werkzeuge struct {
	MaxFileSize            int64
	AllowedFileTypes       []string
	MaxJSONSize            int
	AllowUnknownJSONFields bool
}

// RandomString: generate a random string with the length n from the source randomStringSource

func (w *Werkzeuge) RandomString(n int) string {

	rand.Seed(time.Now().UnixNano())
	sb := strings.Builder{}
	sb.Grow(n)
	for i := 0; i < n; i++ {
		sb.WriteByte(randomStringSource[rand.Intn(len(randomStringSource))])
	}
	return sb.String()
}

//upload files to backend server
//define max size and mime types, the default file size is 1MB
// default behavior allow all file types

// UploadedFile is a struct used to save informations about the files
type UploadedFile struct {
	NewFileName      string
	OriginalFileName string
	FileSize         int64
}

func (w *Werkzeuge) UploadFiles(r *http.Request, uploadDir string, rename ...bool) ([]*UploadedFile, error) {
	renameFile := true
	if len(rename) > 0 {
		renameFile = rename[0]
	}

	var uploadedFiles []*UploadedFile

	if w.MaxFileSize == 0 {
		w.MaxFileSize = 1024 * 1024 //1MB
	}
	err := w.CreateDirIfNotExist(uploadDir)
	if err != nil {
		return nil, err
	}
	err = r.ParseMultipartForm(w.MaxFileSize)
	if err != nil {
		return nil, err
	}

	for _, fHeaders := range r.MultipartForm.File {
		for _, hdr := range fHeaders {
			uploadedFiles, err = func(uploadedFiles []*UploadedFile) ([]*UploadedFile, error) {
				var uploadedFile UploadedFile
				infile, err := hdr.Open()
				if err != nil {
					return nil, err
				}
				defer infile.Close()
				buff := make([]byte, 512)
				_, err = infile.Read(buff)
				if err != nil {
					return nil, err
				}

				allowed := false
				fileType := http.DetectContentType(buff)

				if len(w.AllowedFileTypes) > 0 {
					for _, x := range w.AllowedFileTypes {
						if strings.EqualFold(fileType, x) {
							allowed = true
						}
					}
				} else {
					allowed = true
				}
				if !allowed {
					return nil, errors.New("the uploaded file type is not premitted")
				}
				_, err = infile.Seek(0, 0)
				if err != nil {
					return nil, err
				}

				if renameFile {
					uploadedFile.NewFileName = fmt.Sprintf("%s.%s", w.RandomString(25), filepath.Ext(hdr.Filename))
				} else {
					uploadedFile.NewFileName = hdr.Filename
				}
				var outfile *os.File
				uploadedFile.NewFileName = hdr.Filename
				defer outfile.Close()

				if outfile, err = os.Create(filepath.Join(uploadDir, uploadedFile.NewFileName)); err != nil {
					return nil, err
				} else {
					fileSize, err := io.Copy(outfile, infile)
					if err != nil {
						return nil, err
					}
					uploadedFile.FileSize = fileSize
				}

				uploadedFiles = append(uploadedFiles, &uploadedFile)
				return uploadedFiles, nil
			}(uploadedFiles)
			if err != nil {
				return uploadedFiles, err
			}
		}
	}
	return uploadedFiles, nil
}

func (w *Werkzeuge) UploadFile(r *http.Request, uploadDir string, rename ...bool) (*UploadedFile, error) {
	renameFile := true
	if len(rename) > 0 {
		renameFile = rename[0]
	}

	file, err := w.UploadFiles(r, uploadDir, renameFile)
	if err != nil {
		return nil, err
	}

	return file[0], err
}

func (w *Werkzeuge) CreateDirIfNotExist(path string) error {
	const mode = 0755
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.MkdirAll(path, mode)
		if err != nil {
			return err
		}
	}
	return nil
}

func (w *Werkzeuge) Slugify(s string) (string, error) {
	if s == "" {
		return "", errors.New("empty string not permitted")
	}

	var re = regexp.MustCompile(`[^a-z\d]+`)
	slug := strings.Trim(re.ReplaceAllString(strings.ToLower(s), "-"), "-")

	if len(slug) == 0 {
		return "", errors.New("after removing characters,slug is zero length")
	}

	return slug, nil
}

func (w *Werkzeuge) DownloadStaticFile(wr http.ResponseWriter, r *http.Request, p, file, displayName string) {
	fp := path.Join(p, file)
	wr.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s\"", displayName))
	http.ServeFile(wr, r, fp)
}

type JSONResponse struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
	Data    any    `json:"data,omitempty"`
}

func (w *Werkzeuge) ReadJSON(wr http.ResponseWriter, r *http.Request, data any) error {
	maxBytes := 1024 * 1024

	if w.MaxJSONSize == 0 {
		w.MaxJSONSize = maxBytes
	}

	r.Body = http.MaxBytesReader(wr, r.Body, int64(w.MaxJSONSize))

	dec := json.NewDecoder(r.Body)
	if !w.AllowUnknownJSONFields {
		dec.DisallowUnknownFields()
	}
	err := dec.Decode(data)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError
		var invalidUnmarshalError *json.InvalidUnmarshalError

		switch {
		case errors.As(err, &syntaxError):
			return fmt.Errorf("body contains badly-formed JSON (at character %d)", syntaxError.Offset)
		case errors.Is(err, io.ErrUnexpectedEOF):
			return fmt.Errorf("body contains badly-formed JSON")
		case errors.As(err, &unmarshalTypeError):
			if unmarshalTypeError.Field != "" {
				return fmt.Errorf("body contains incorrect JSOn type for field %q", unmarshalTypeError.Field)
			}
			return fmt.Errorf("body contains incorrect JSON type (at character %d)", unmarshalTypeError.Offset)
		case errors.Is(err, io.EOF):
			return fmt.Errorf("body must not be empty")
		case strings.HasPrefix(err.Error(), "json: unknown field"):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field")
			return fmt.Errorf("body contains unknown key %s", fieldName)
		case err.Error() == "http: request body to large":
			return fmt.Errorf("body must not be larger than %d Bytes", w.MaxJSONSize)
		case errors.As(err, &invalidUnmarshalError):
			return fmt.Errorf("error unmarshalling JSON: %s", err.Error())

		default:
			return err
		}

	}

	// Check for more than one JSON file

	err = dec.Decode(&struct{}{})
	//TODO: != or == ?
	if err != io.EOF {
		return errors.New("Body must contain only one json file")
	}
	return nil
}

func (w *Werkzeuge) WriteJSON(wr http.ResponseWriter, status int, data any, headers ...http.Header) error {
	out, err := json.Marshal(data)
	if err != nil {
		return err
	}

	if len(headers) > 0 {
		for key, value := range headers[0] {
			wr.Header()[key] = value
		}
	}
	wr.Header().Set("Content-Type", "application/json")
	wr.WriteHeader(status)
	_, err = wr.Write(out)
	if err != nil {
		return err
	}

	return nil
}

func (w *Werkzeuge) ErrorJSON(wr http.ResponseWriter, err error, status ...int) error {
	statusCode := http.StatusBadRequest

	if len(status) > 0 {
		statusCode = status[0]
	}

	var payloud JSONResponse
	payloud.Error = true
	payloud.Message = err.Error()

	return w.WriteJSON(wr, statusCode, payloud)
}

func (w *Werkzeuge) PushJSONToRemote(url string, data interface{}, client ...*http.Client) (*http.Response, int, error) {

	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, 0, err
	}

	httpClient := &http.Client{}
	if len(client) > 0 {
		httpClient = client[0]
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, 0, err
	}
	request.Header.Set("Content-Type", "application/json")
	response, err := httpClient.Do(request)
	if err != nil {
		return nil, http.StatusBadRequest, err
	}
	defer response.Body.Close()
	return response, http.StatusOK, nil

}
